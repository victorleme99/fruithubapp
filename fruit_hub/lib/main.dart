import 'package:flutter/material.dart';
import 'package:fruit_hub/pages/details/details.dart';
import 'package:fruit_hub/pages/home_page/home_page.dart';
import 'package:fruit_hub/pages/login/login_page.dart';
import 'package:fruit_hub/pages/login/password_page.dart';
import 'package:fruit_hub/pages/on_boarding.dart';
import 'package:fruit_hub/pages/splash_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fruit Hub',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      initialRoute: '/details',
      routes: {
        '/splash':(_) => const SplashPage(),
        '/onBoarding': (_) => const OnBoardingPage(),
        '/login': (_) => const LoginPage(),
        '/password': (_) => const PasswordPage(),
        '/home': (_) => const HomePage(),
        '/details': (_) => const DetailsPage(),
      },
    );
  }
}
