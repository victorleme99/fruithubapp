import 'package:flutter/material.dart';
import 'package:fruit_hub/utils/custom_colors.dart';

class CustomButton extends StatefulWidget {
  final String textContent;
  final double sizeButton;
  
  const CustomButton({Key? key, required this.textContent, required this.sizeButton}) : super(key: key);

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ElevatedButton(
      onPressed: () {},
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(CustomColors.themeColor),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        )),
        fixedSize: MaterialStateProperty.all(Size(size.width * widget.sizeButton, 56)),
      ),
      child: Text(
        widget.textContent,
        style: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
      ),
    );
  }
}
