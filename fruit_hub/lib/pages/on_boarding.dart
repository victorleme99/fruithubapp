import 'package:flutter/material.dart';
import 'package:fruit_hub/utils/custom_colors.dart';

class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        children: [
          Container(
              color: CustomColors.themeColor,
              height: size.height * 0.60,
              width: size.width,
              child: Stack(
                children: [
                  // Small fruits on basket's top-right
                  Positioned(
                    top: size.height * 0.1,
                    right: size.width * 0.1,
                    child: const Image(
                      image: AssetImage(
                        'assets/images/fruitDropsOnBoarding.png',
                      ),
                      alignment: Alignment.topRight,
                    ),
                  ),
                  //Fruit basket image
                  Positioned(
                    top: size.height * 0.1,
                    left: size.width * 0.08,
                    child: const Image(
                      image:
                          AssetImage('assets/images/fruitBasketOnBoarding.png'),
                      alignment: Alignment.center,
                    ),
                  ),
                  // Elipse under basket's image
                  Positioned(
                    bottom: size.height * 0.02,
                    left: size.width * 0.08,
                    child: const Image(
                      image: AssetImage('assets/images/elipseOnBoarding.png'),
                      alignment: Alignment.bottomCenter,
                    ),
                  )
                ],
              )),
          const SizedBox(
            height: 20,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 18),
            padding: const EdgeInsets.only(top: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Get The Freshest Fruit Salad Combo',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: Color.fromRGBO(39, 33, 77, 1)),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  'We deliver the best and freshest fruit salad in town. Order for a combo today!!!',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: Color.fromARGB(120, 93, 87, 126)),
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/login');
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(CustomColors.themeColor),
                      fixedSize:
                          MaterialStateProperty.all(Size(size.width, 56)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ))),
                  child: const Text("Let's Continue",
                      style: TextStyle(color: Colors.white)),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
