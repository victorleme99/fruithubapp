import 'package:flutter/material.dart';
import 'package:fruit_hub/utils/custom_colors.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3)).then(
      (value) => Navigator.of(context).pushReplacementNamed('/onBoarding'),
    );
    // Future.wait(
    //   ,
    // ).then((value) => Navigator.of(context).pushReplacementNamed('/onBoarding'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Image(image: AssetImage('assets/images/splash.png')),
            CircularProgressIndicator(
              color: CustomColors.themeColor,
            ),
          ],
        ),
      ),
    );
  }
}
