import 'package:flutter/material.dart';

class ComboModel {
  final String linkImage;
  final String comboName;
  final double price;
  final Color? backgroundColor;

  ComboModel(this.linkImage, this.comboName, this.price, this.backgroundColor);
}
