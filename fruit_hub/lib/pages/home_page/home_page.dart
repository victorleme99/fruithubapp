import 'package:flutter/material.dart';
import 'package:fruit_hub/pages/home_page/cards/cards.dart';
import 'package:fruit_hub/pages/home_page/models/combos_models.dart';
import 'package:fruit_hub/pages/home_page/tab_bar/tabbar_personalized.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  List<ComboModel> combos = [
    ComboModel(
        'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
        'Honey lime combo',
        45.00, null),
    ComboModel(
        'assets/images/homePage/foods/Glowing-Berry-Fruit-Salad-8-720x720-removebg-preview 1.png',
        'Berry mango combo',
        45.00, null),
    ComboModel(
        'assets/images/homePage/foods/Glowing-Berry-Fruit-Salad-8-720x720-removebg-preview 1.png',
        'Berry mango combo',
        45.00, null),
  ];

  @override
  Widget build(BuildContext context) {
    //TabController controllerTab = TabController(length: 4, vsync: this);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            //Padding with the title and image on the top-right of the page
            Padding(
              padding: EdgeInsets.only(
                  top: size.height * 0.03,
                  left: size.width * 0.03,
                  right: size.width * 0.03,
                  bottom: size.height * 0.01),
              child: (Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: RichText(
                        text: const TextSpan(
                      text: 'Hello Stephen, ',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'what fruit salad combo do you want today?',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    )),
                  ),
                  SizedBox(
                    width: size.width * 0.01,
                  ),
                  const Image(
                      image: AssetImage(
                          'assets/images/homePage/basketShoppingCart.png')),
                ],
              )),
            ),

            SizedBox(
              height: size.height * 0.05,
            ),

            //Padding with the search bar and filter search image
            Padding(
              padding: EdgeInsets.only(
                  left: size.width * 0.05, right: size.width * 0.05),
              child: Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        prefixIcon: const Icon(Icons.search),
                        hintText: 'Search for fruit salad combo',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: size.width * 0.03),
                  const Image(
                      image:
                          AssetImage('assets/images/homePage/searchFilter.png'))
                ],
              ),
            ),

            SizedBox(
              height: size.height * 0.05,
            ),

            //Container with the title "Recommended Combo"
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: size.width * 0.03, vertical: size.width * 0.05),
              alignment: Alignment.centerLeft,
              child: const Text(
                'Recommended Combo',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),

            SizedBox(
              height: size.height * 0.03,
            ),

            //Horizontal listview with some examples of combos
            SizedBox(
              height: 200,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: combos.length,
                itemBuilder: (context, int index) {
                  return Padding(
                    padding: EdgeInsets.only(
                        left: index == 0 ? 15 : 0,
                        right: index == combos.length - 1 ? 15 : 0),
                    child: Cards(
                        image: combos[index].linkImage,
                        name: combos[index].comboName,
                        price: combos[index].price,
                        backgroundColor: null,
                        ),
                  );
                },
                separatorBuilder: (context, int index) {
                  return const SizedBox(
                    width: 25,
                  );
                },
              ),
            ),

            SizedBox(
              height: size.height * 0.05,
            ),

            const SizedBox(
              height: 270,
              //height: MediaQuery.of(context).size.height,
              child: TabBarPersonalized(),
            ),

            SizedBox(
              height: size.height * 0.05,
            ),
            //TabBar

            /*TabBarCreated(controller: controllerTab),
            TabBarContent(controllerContent: controllerTab),*/

            // const TabBarView(children: [
            //   Cards(image: 'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png', name: 'Honey lime combo', price: 45.00,),
            //   SizedBox(width: 25,),

            //   Cards(image: 'assets/images/homePage/foods/Glowing-Berry-Fruit-Salad-8-720x720-removebg-preview 1.png', name: 'Berry mango combo', price: 30.00),
            //       SizedBox(width: 25,),
            // ])
          ],
        ),
      ),
    );
  }
}
