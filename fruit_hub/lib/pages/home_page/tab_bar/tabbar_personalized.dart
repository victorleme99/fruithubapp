import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fruit_hub/pages/home_page/cards/combos_variety.dart';
import 'package:fruit_hub/utils/custom_colors.dart';

import '../cards/cards.dart';

class TabBarPersonalized extends StatefulWidget {
  const TabBarPersonalized({Key? key}) : super(key: key);

  @override
  State<TabBarPersonalized> createState() => _TabBarPersonalizedState();
}

class _TabBarPersonalizedState extends State<TabBarPersonalized> {
  CombosVariety combos = CombosVariety();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          toolbarHeight: 0.0,
          toolbarOpacity: 0.0,
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          bottom: const TabBar(
            indicatorColor: Color.fromARGB(255, 255, 165, 81),
            labelColor: Colors.black,
            labelStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
            unselectedLabelColor: Colors.grey,
            unselectedLabelStyle: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.normal,
            ),
            tabs: [
              Text('Hottest'),
              Text('Popular'),
              Text('New combo'),
              Text('Top'),
            ],
            
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: TabBarView(
            children: [
              //Horizontal listview with some examples of combos
              SizedBox(
                height: 200,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: combos.combosHottest.length,
                  itemBuilder: (context, int index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: index == 0 ? 15 : 0,
                          right: index == combos.combosHottest.length - 1 ? 15 : 0),
                      child: Cards(
                          image: combos.combosHottest[index].linkImage,
                          name: combos.combosHottest[index].comboName,
                          price: combos.combosHottest[index].price,
                          backgroundColor: combos.combosHottest[index].backgroundColor,
                        ),
                    );
                  },
                  separatorBuilder: (context, int index) {
                    return const SizedBox(
                      width: 25,
                    );
                  },
                ),
              ),
              //Icon(Icons.directions_car),
              //Horizontal listview with some examples of combos
              SizedBox(
                height: 200,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: combos.combosPopular.length,
                  itemBuilder: (context, int index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: index == 0 ? 15 : 0,
                          right: index == combos.combosPopular.length - 1 ? 15 : 0),
                      child: Cards(
                          image: combos.combosPopular[index].linkImage,
                          name: combos.combosPopular[index].comboName,
                          price: combos.combosPopular[index].price,
                          backgroundColor: combos.combosPopular[index].backgroundColor,
                          ),
                    );
                  },
                  separatorBuilder: (context, int index) {
                    return const SizedBox(
                      width: 25,
                    );
                  },
                ),
              ),
              //Icon(Icons.directions_transit),
              //Horizontal listview with some examples of combos
              SizedBox(
                height: 200,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: combos.combosNewCombos.length,
                  itemBuilder: (context, int index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: index == 0 ? 15 : 0,
                          right: index == combos.combosNewCombos.length - 1 ? 15 : 0),
                      child: Cards(
                          image: combos.combosNewCombos[index].linkImage,
                          name: combos.combosNewCombos[index].comboName,
                          price: combos.combosNewCombos[index].price,
                          backgroundColor: combos.combosNewCombos[index].backgroundColor,
                          ),
                    );
                  },
                  separatorBuilder: (context, int index) {
                    return const SizedBox(
                      width: 25,
                    );
                  },
                ),
              ),
              //Icon(Icons.directions_bike),
              //Horizontal listview with some examples of combos
              SizedBox(
                height: 200,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: combos.combosTop.length,
                  itemBuilder: (context, int index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: index == 0 ? 15 : 0,
                          right: index == combos.combosTop.length - 1 ? 15 : 0),
                      child: Cards(
                          image: combos.combosTop[index].linkImage,
                          name: combos.combosTop[index].comboName,
                          price: combos.combosTop[index].price,
                          backgroundColor: combos.combosTop[index].backgroundColor,
                          ),
                    );
                  },
                  separatorBuilder: (context, int index) {
                    return const SizedBox(
                      width: 25,
                    );
                  },
                ),
              ),
              //Icon(Icons.directions_bike),
            ]
          ),
        ),
      ),
    );
  }
}
