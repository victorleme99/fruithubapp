import 'dart:core';

import 'package:flutter/material.dart';

import '../models/combos_models.dart';

class CombosVariety {
  static Color colorHottest = const Color.fromARGB(10, 254, 135, 37);
  static Color colorPopular = const Color.fromARGB(10, 33, 240, 1);
  static Color colorTop = const Color.fromARGB(10, 1, 75, 246);
  static Color colorNewCombo = const Color.fromARGB(10, 242, 1, 250);

  List<ComboModel> combosHottest = [
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorHottest,
    ),
    ComboModel(
      'assets/images/homePage/foods/Glowing-Berry-Fruit-Salad-8-720x720-removebg-preview 1.png',
      'Berry mango combo',
      45.00,
      colorHottest,
    ),
    ComboModel(
      'assets/images/homePage/foods/Glowing-Berry-Fruit-Salad-8-720x720-removebg-preview 1.png',
      'Berry mango combo',
      45.00,
      colorHottest,
    ),
  ];

  List<ComboModel> combosPopular = [
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorPopular,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorPopular,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorPopular,
    ),
  ];

  List<ComboModel> combosTop = [
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorTop,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorTop,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorTop,
    ),
  ];

  List<ComboModel> combosNewCombos = [
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorNewCombo,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorNewCombo,
    ),
    ComboModel(
      'assets/images/homePage/foods/Honey-Lime-Peach-Fruit-Salad-3-725x725-1-removebg-preview 1.png',
      'Honey lime combo',
      45.00,
      colorNewCombo,
    ),
  ];
}
