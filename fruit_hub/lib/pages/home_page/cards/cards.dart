import 'package:flutter/material.dart';

class Cards extends StatelessWidget {
  final String name, image;
  final double price;
  final Color? backgroundColor;

  const Cards(
      {Key? key, required this.image, required this.name, required this.price, required this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.03),
      height: 200,
      width: 200,
      decoration: BoxDecoration(
        color: backgroundColor ?? Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(32, 32, 32, 0.05),
            offset: Offset.zero,
            blurRadius: 2,
          ),
        ],
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 8,
          ),
          Center(child: Image(image: AssetImage(image))),
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.03),
            child: Text(
              name,
              softWrap: false,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w300,
              ),
              overflow: TextOverflow.fade,
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.03),
            child: Text(
              'R\$${price.toStringAsPrecision(4)}',
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(240, 134, 38, 1),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
