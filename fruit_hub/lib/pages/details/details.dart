import 'package:flutter/material.dart';
import 'package:fruit_hub/utils/custom_buttom.dart';
import 'package:fruit_hub/utils/custom_colors.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.themeColor,
      appBar: AppBar(
        elevation: 0,
        leading: const Image(image: AssetImage('assets/images/detailsPage/back.png')),
        actions: const [
          Image(image: AssetImage('assets/images/detailsPage/shoppingBasket.png'), alignment: Alignment.center,),
        ],
      ),
      body: SizedBox(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: size.height*0.08),
              child: const Image(
                image: AssetImage('assets/images/foods/quinoaFruitSalad.png'),
                alignment: Alignment.topCenter,
              ),
            ),
            Expanded(
              child: Container(
                width: size.width,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35),
                    topRight: Radius.circular(35)
                  )
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Title combo name
                      Container(
                        alignment: Alignment.topLeft,
                        margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
                        child: const Text(
                          'Quinoa Fruit Salad',
                          style: TextStyle(
                            fontSize: 32,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      // Price and quantity
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Image(
                                  image: AssetImage('assets/images/detailsPage/minusSign.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                  child: const Text('1',
                                    style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                const Image(
                                  image: AssetImage('assets/images/detailsPage/plusSign.png'),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                            child: const Text(
                              "R\$35.00",
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: size.height*0.03,),
                      // Grey line (divider)
                      Container(
                        height: 1,
                        width: size.width,
                        color: const Color.fromARGB(255, 209, 203, 203),
                      ),
                      SizedBox(height: size.height*0.02,),
                      // Title "One pack contains"
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: size.width*0.02, vertical: size.height*0.01),
                        alignment: Alignment.centerLeft,
                        child: const Text(
                          'One pack contains:',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      // Orange line (divider)
                      Container(
                        alignment: Alignment.bottomLeft,
                        height: 1,
                        color: CustomColors.themeColor,
                        width: 180,
                      ),
                      SizedBox(height: size.height*0.02),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: size.width*0.02),
                        child: const Text(
                          'Red quinoa, lime, honey, blueberries, strawberries, mango and fresh mint.',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                          ),
                        ),
                      ),
                      SizedBox(height: size.height*0.03,),
                      // Grey line (divider)
                      Container(
                        height: 1,
                        width: size.width,
                        color: const Color.fromARGB(255, 209, 203, 203),
                      ),
                      SizedBox(height: size.height*0.03,),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: size.width*0.02),
                        child: const Text(
                          'If you are looking for a new fruit salad to eat today, quinoa is the perfect brunch for you.',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      SizedBox(height: size.height*0.03,),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:const [
                            Image(image: AssetImage('assets/images/detailsPage/favouriteButton.png')),
                            CustomButton(textContent: 'Add to basket', sizeButton: 0.7),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ),
            ),
          ]
        ),
      ),
    );
  }
}
